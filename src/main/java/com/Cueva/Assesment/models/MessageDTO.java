package com.Cueva.Assesment.models;

import java.util.Date;

public class MessageDTO {
    private Long id;
    private String content;
    private Date date;
    private String username1;
    private String username2;

    public MessageDTO(Long id, String content, Date date, String username1, String username2) {
        this.id = id;
        this.content = content;
        this.date = date;
         this.username2 = username2;
        this.username1 = username1;

    }

    public MessageDTO() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername1() {
        return username1;
    }

    public void setUsername1(String username1) {
        this.username1 = username1;
    }

    public String getUsername2() {
        return username2;
    }

    public void setUsername2(String username2) {
        this.username2 = username2;
    }
}
