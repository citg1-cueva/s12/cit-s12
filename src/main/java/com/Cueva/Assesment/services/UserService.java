package com.Cueva.Assesment.services;

import com.Cueva.Assesment.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    void createUser(User user);
    Iterable<User> getUsers();
    Iterable<String> getUsernames(String username);
    void loginUser(User user) throws Exception;
    Long getIDByUsername(String username);
    ResponseEntity deleteUser(Long id);
    ResponseEntity updateUser(Long id, User user);
}
