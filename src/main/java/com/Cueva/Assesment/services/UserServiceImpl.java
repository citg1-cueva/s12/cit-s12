package com.Cueva.Assesment.services;

import com.Cueva.Assesment.models.User;
import com.Cueva.Assesment.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    // Create user
    public void createUser(User user) {
        userRepository.save(user);
    }

    // Get users
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }


    public Iterable<String> getUsernames(String username){
        List<String> usernames = new ArrayList<>();
        Iterable<User> users = userRepository.findAll();
        for (User user : users) {
            if(!user.getUsername().equals(username))
            usernames.add(user.getUsername());
        }
        return usernames;
    }

    @Override
    public void loginUser(User user) throws Exception {
        User foundUser = userRepository.findByUsername(user.getUsername());

        if (foundUser == null || !foundUser.getPassword().equals(user.getPassword())) {
            throw new Exception("Incorrect username or password.");
        }

        // Login successful
    }

    @Override
    public Long getIDByUsername(String username) {
        return  userRepository.findByUsername(username).getId();
    }

    // Delete user
    public ResponseEntity deleteUser(Long id) {
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
    }

    // Update user
    public ResponseEntity updateUser(Long id, User user) {
        User userForUpdating = userRepository.findById(id).get();

        userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());
        userRepository.save(userForUpdating);
        return new ResponseEntity<>("User updated successfully", HttpStatus.OK);

    }

}
