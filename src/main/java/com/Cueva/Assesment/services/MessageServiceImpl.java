package com.Cueva.Assesment.services;


import com.Cueva.Assesment.models.Message;
import com.Cueva.Assesment.models.MessageDTO;
import com.Cueva.Assesment.repositories.MessageRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public void createMessage(Message message) {
        messageRepository.save(message);
    }

    @Override
    public List<Message> getAllMessages() {
        return (List<Message>) messageRepository.findAll();
    }

    @Override
    public void deleteMessage(Long id) {
        messageRepository.deleteById(id);
    }
    @Override
    public List<Message> getMessagesBySenderId(Long userId) {
        return messageRepository.findBySenderId(userId);
    }

    @Override
    public List<MessageDTO> getMessagesContentBySenderId(Long userId1, Long userId2) {
        List<Message> messages1 = messageRepository.findBySenderId(userId1);
        List<Message> messages2 = messageRepository.findBySenderId(userId2);
        messages1.addAll(messages2);
        List<MessageDTO> messageDTOs = new ArrayList<>();
        for (Message message : messages1) {
            if(message.getSender().getId() == userId1 && message.getReceiver().getId() == userId2 || message.getReceiver().getId() == userId1 && message.getSender().getId() == userId2){
                String username1 = message.getSender().getUsername();
                String username2 = message.getReceiver().getUsername();// Assuming User entity has a 'username' field
                MessageDTO messageDTO = new MessageDTO(message.getId(), message.getContent(), message.getSentDate(), username1 , username2);
                messageDTOs.add(messageDTO);
            }
        }

        return messageDTOs;
    }


    @Override
    public List<MessageDTO> getContetnBySenderId(Long userId) {
        List<Message> messages = messageRepository.findBySenderId(userId);
        List<MessageDTO> messageDTOs = new ArrayList<>();

        for (Message message : messages) {
            String username1 = message.getSender().getUsername();
            String username2 = message.getReceiver().getUsername();
            MessageDTO messageDTO = new MessageDTO(message.getId(), message.getContent(), message.getSentDate(), username1 , username2);
            messageDTOs.add(messageDTO);
        }

        return messageDTOs;
    }



    @Override
    public List<MessageDTO> getPreviousMessages(Long userId) {
        List<Message> messages = messageRepository.findBySenderId(userId);
        List<MessageDTO> messageDTOs = new ArrayList<>();
        Set<String> uniqueUsernames = new HashSet<>(); // Set to store unique usernames
        String username1 = "";
        String username2 = "";

        // Sort the messages by date
        messages.sort(Comparator.comparing(Message::getSentDate));
        Collections.sort(messages, Comparator.comparing(Message::getSentDate).reversed());

        for (Message message : messages) {
            if (message.getReceiver().getId() == userId) {
                username1 = message.getSender().getUsername();
                username2 = message.getReceiver().getUsername();
            } else {
                username2 = message.getSender().getUsername();
                username1 = message.getReceiver().getUsername();
            }

            if (!uniqueUsernames.contains(username1)) {
                MessageDTO messageDTO = new MessageDTO(message.getId(), message.getContent(), message.getSentDate(), username1, username2);
                messageDTOs.add(messageDTO);
                uniqueUsernames.add(username1);
            }
        }

        // Sort the messageDTOs by date in descending order
        Collections.sort(messageDTOs, Comparator.comparing(MessageDTO::getDate).reversed());

        return messageDTOs;
    }






    @Override
    public void updateMessage(Long id, Message message) {
        Optional<Message> optionalMessage = messageRepository.findById(id);
        if (optionalMessage.isPresent()) {
            Message existingMessage = optionalMessage.get();
            existingMessage.setContent(message.getContent());
            existingMessage.setSender(message.getSender());

            messageRepository.save(existingMessage);
        }
    }
}