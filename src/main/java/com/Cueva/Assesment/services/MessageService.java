package com.Cueva.Assesment.services;

import com.Cueva.Assesment.models.Message;
import com.Cueva.Assesment.models.MessageDTO;

import java.util.ArrayList;
import java.util.List;

public interface MessageService {
    void createMessage(Message message);
    List<Message> getAllMessages();
    List<Message> getMessagesBySenderId(Long userId);
    List<MessageDTO> getMessagesContentBySenderId(Long userId1, Long userId2);
    List<MessageDTO> getContetnBySenderId(Long userId);
    List<MessageDTO> getPreviousMessages(Long userId);
    void deleteMessage(Long id);
    void updateMessage(Long id, Message message);
}