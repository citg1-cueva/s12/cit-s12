package com.Cueva.Assesment.repositories;



import com.Cueva.Assesment.models.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {
    List<Message> findBySenderId(Long userId);

}
