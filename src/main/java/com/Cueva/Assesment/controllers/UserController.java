package com.Cueva.Assesment.controllers;

import com.Cueva.Assesment.models.User;
import com.Cueva.Assesment.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    // Create user
    @RequestMapping(value="/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity<>("User created successfully", HttpStatus.CREATED);
    }
    @RequestMapping(value="/Login", method = RequestMethod.POST)
    public ResponseEntity<Object> loginUser(@RequestBody User user) throws Exception {
        userService.loginUser(user);
        return new ResponseEntity<>("Log In successfully", HttpStatus.CREATED);
    }


    // Get users
    @RequestMapping(value="/users", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @RequestMapping(value="/usernames/{username}", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsernames(@PathVariable String username) {
        return new ResponseEntity<>(userService.getUsernames(username), HttpStatus.OK);
    }
    @RequestMapping(value="/users/{username}", method = RequestMethod.GET)
    public Long getIDByUsername(@PathVariable String username) {
        return  userService.getIDByUsername(username);
    }
    // Delete user
    @RequestMapping(value = "/users/{userid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable Long userid) {
        return userService.deleteUser(userid);
    }

    // Update user
    @RequestMapping(value="/users/{userid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable Long userid, @RequestBody User user) {
        return userService.updateUser(userid, user);
    }

}
