package com.Cueva.Assesment.controllers;


import com.Cueva.Assesment.models.Message;
import com.Cueva.Assesment.models.MessageDTO;
import com.Cueva.Assesment.services.MessageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/messages")
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping
    public ResponseEntity<Object> createMessage(@RequestBody Message message) {
        messageService.createMessage(message);
        return new ResponseEntity<>("Message created successfully.", HttpStatus.CREATED);
    }
    @GetMapping("/sender/{userId}")
    public ResponseEntity<List<Message>> getMessagesBySenderId(@PathVariable Long userId) {
        List<Message> messages = messageService.getMessagesBySenderId(userId);
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }
    @GetMapping
    public ResponseEntity<List<Message>> getAllMessages() {
        List<Message> messages = messageService.getAllMessages();
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }
    @GetMapping("/content/{userId}")
    public ResponseEntity<List<MessageDTO>> getContentBySenderId(@PathVariable Long userId) {
        List<MessageDTO> messageDTOs = messageService.getContetnBySenderId(userId);
        return new ResponseEntity<>(messageDTOs, HttpStatus.OK);
    }

    @GetMapping("/content/previous/{userId}")
    public ResponseEntity<List<MessageDTO>> getPreviousMessagesbyId(@PathVariable Long userId) {
        List<MessageDTO> messageDTOs = messageService.getPreviousMessages(userId);
        return new ResponseEntity<>(messageDTOs, HttpStatus.OK);
    }

    @GetMapping("/content/{userId1}/{userId2}")
    public ResponseEntity<List<MessageDTO>> getMessagesContentBySenderId(@PathVariable Long userId1,@PathVariable Long userId2) {
        List<MessageDTO> messageDTOs = messageService.getMessagesContentBySenderId(userId1,userId2);
        return new ResponseEntity<>(messageDTOs, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteMessage(@PathVariable Long id) {
        messageService.deleteMessage(id);
        return new ResponseEntity<>("Message deleted successfully.", HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateMessage(@PathVariable Long id, @RequestBody Message message) {
        messageService.updateMessage(id, message);
        return new ResponseEntity<>("Message updated successfully.", HttpStatus.OK);
    }
}